# YandeSpider



## 地址规律

收藏处获取的地址有以下组合
image-jpg
sample-jpg
jpeg-jpg

二级页面内获取的链接有以下组合
image-png
image-jpg
sample-jpg

对sample-jpg要进二级页面处理。
因为png文件会根据实际自动变成jpg文件，所以对image-jpg和jpeg-jpg则暴力将jpeg转成image，将jpg转成png。
